/**
 * Задача 3.
 *
 * Напишите функции compose(), которая в качестве аргументов принимает неограниченное количество функций.
 *
 * При запуске compose() последовательно запускает коллбек-функции из аргументов.
 *
 * Важно: compose() выполняет коллбек-функции из аргументов НАЧИНАЯ С КОНЦА.
 *
 * Каждая коллбек-функция из цепочки в качестве своего аргумента принимает то, что возвращает предыдущая коллбек-функция.
 * То есть возвращаемое значение каждой коллбек-функции из цепочки
 * становится доступным из параметра следующей коллбек-функции в цепочке.
 *
 * Функция compose() возвращает ещё одну функцию с одним параметром.
 * Значение, переданное этой функции в качестве аргумента должно стать
 * параметром первой коллбек-функции в цепочке выполнения функции compose().
 *
 *
 * Генерировать ошибки если:
 * - Любой из аргументов не является функцией;
 * - Любая функция из аргументов не вернула значение.
 *
 * Заметка:
 * Если функции, которая является возвращаемым значением compose()
 * не передать в качестве аргумента какое-либо значение, генерировать ошибку не нужно.
 */

// Решение
function funcValidation(fn) {
    if (typeof fn !== 'function') {
        throw new TypeError(`arg is not a function`);
    }
}

function checkEmptyCb(fn) {
    if (typeof fn() === 'undefined') {
        throw new Error(`callback did not return any value`);
    }
}

function compose(...rest) {
    let res = '';
    // res = rest.reverse().reduce((accum, currentValue) => res + currentValue(accum), res)
    res = rest.reverse().reduce((accum, currentValue) => {
        funcValidation(currentValue);
        checkEmptyCb(currentValue);
        return res + currentValue(accum);
    }, res);
    return (a = '') => {
        return a + res;
    };
}

const result1 = compose(
  prevResult => prevResult + 'o',
  prevResult => prevResult + 'l',
  prevResult => prevResult + 'l',
  prevResult => prevResult + 'e',
)('h');
const result2 = compose(
  prevResult => prevResult + 'o',
  prevResult => prevResult + 'l',
  prevResult => prevResult + 'l',
  prevResult => prevResult + 'e',
  () => 'h',
)();

console.log(result1); // 'hello'
console.log(result2); // 'hello'
