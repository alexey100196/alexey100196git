/**
 * Напишите функцию для форматирования даты.
 *
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 *
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
 */

const formatDate = (date, format, delimiter = '.') => {
    if (delimiter !== '-' && delimiter !== '.' && delimiter !== '/') {
        delimiter = '.';
    }
    const resDate = [];
    const formatArr = format.split(' ')
    try {
        for (let i = 0; i < formatArr.length; i++) {
            switch (formatArr[i].toUpperCase()) {
                case 'DD':
                    resDate.push(date.getDate());
                    break;
                case 'MM':
                    resDate.push(date.getMonth());
                    break;
                case 'YYYY':
                    resDate.push(date.getFullYear());
                    break;
                default:
                    throw new SyntaxError(`wrong format ${formatArr[i]}`);
            }
        }
    } catch (e) {
        console.log(e.message);
    }
    return resDate.join(delimiter);
};

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD mm', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '')); // 2021
console.log(formatDate(new Date(2021, 10, 22), 'dd YYYY', '/')); // 22/2021
console.log(formatDate(new Date(2021, 10, 22), 'MM DD YYYY', '.')); // 10.22.2021

