/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ
//F n = F n-1 + F n-2
function createFibonacciGenerator() {
    let sequence = [0, 1];
    return {
        print() {
            const f0 = sequence[sequence.length - 2];
            const f1 = sequence[sequence.length - 1];
            const num = f0 + f1;

            sequence[sequence.length] = num;

            return sequence[sequence.length - 2];
        },
        reset() {
            sequence = [0, 1];
        }
    };
}

const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2



