/**
 * Задача 7.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 *
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

// РЕШЕНИЕ
function getDivisors(number) {
    if (typeof number !== 'number' || number < 1) {
        throw new Error('error');
    }
    let num = number;
    const res = [];
    for (let i = 0; i < number; i++) {
        let divider = number / num--;
        if (Number.isInteger(divider)) {
            res.push(divider);
        }
    }
    return res;
}
console.log(getDivisors(12)); // [1, 2, 3, 4, 6, 12]
console.log(getDivisors(13)); // [1, 13]
console.log(getDivisors(367)); // [1, 367]


