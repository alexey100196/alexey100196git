/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f(...number) {
    // const args = [].slice.call(arguments);
    // const rest = Array.from(arguments);

    if (number.some(item => typeof item !== 'number')) {
        throw new Error('not a number');
    }
    const sum = number.reduce((acum, current) => acum + current);
    return sum;
}
console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
