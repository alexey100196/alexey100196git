/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    let result = typeof str !== 'string' ? null : str[0].toUpperCase() + str.slice(1);
    return result;
}

console.log(upperCaseFirst('pitter')); // Pitter
