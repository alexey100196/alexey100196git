/**
 * Задача 3.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 */

function truncate(string, maxLength) {
    let result = string;
    if (typeof string !== 'string' || typeof maxLength !== 'number') {
        return console.log('wrong type');
    }
    else if (string.length > maxLength) {
        console.log('усечённая строка:', string);
        result = string.slice(0, maxLength - 3) + '...';
    }
    return result;
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'



