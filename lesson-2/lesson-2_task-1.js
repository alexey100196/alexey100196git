// Перепишите код, заменив оператор 'if' на тернарный оператор '?'

////////////////// Задание //////////////////
// const a = 2;
// const b = 1;
// let result = null;
//
// if (a + b < 4) {
//     result = true;
// } else {
//     result = false;
// }
//
// console.log(result);

////////////////// Решение //////////////////
const a = 2;
const b = 1;
let result = null;

result = a + b < 4;

// result = Boolean(a + b < 4); //v2
// result = (a + b < 4) ? Boolean(true) : Boolean(false); //v3
// result = (a + b <= 4) || false; //v4
// result = (a + b) < 4 ? true : false; //v5
// result = (a + b < 4) ? true : false; //v6
// result = a + b < 4 ? true : false; //v7

console.log(result);
