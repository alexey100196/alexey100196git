// Перепишите `if..else` с использованием нескольких операторов `?`.
// Для читаемости — оформляйте код в несколько строк.

////////////////// Задание //////////////////
// let message;

// if (login === 'Pitter') {
//   message = 'Hi';
// } else if (login === 'Owner') {
//   message = 'Hello';
// } else if (login === '') {
//   message = 'unknown';
// } else {
//   message = '';
// }

////////////////// Решение //////////////////

let message;

let login = 'nothing';
message = login === 'Pitter' ? 'Hi' :
  login === 'Owner' ? 'Hello' :
    login === '' ? 'unknown'
      : '';

console.log(message);
