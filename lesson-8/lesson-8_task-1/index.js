/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
 */

// РЕШЕНИЕ
const form = document.getElementById('form');

const formGroupInputEmail = document.createElement('div');
formGroupInputEmail.classList.add('form-group');
const formGroupInputPass = document.createElement('div');
formGroupInputPass.classList.add('form-group');
const formGroupCheckbox = document.createElement('div');
formGroupCheckbox.classList.add('form-group', 'form-check');
const formGroupBtn = document.createElement('div');
formGroupBtn.classList.add('form-group');

function createLabel(attrFor, text, className) {
    const label = document.createElement('label');
    label.classList.add(className);
    label.setAttribute('for', attrFor);
    label.innerText = text;
    return label;
}

function createInput(id, type, placeholder, className = 'form-control') {
    const input = document.createElement('input');
    input.classList.add(className);
    input.setAttribute('id', id);
    input.setAttribute('type', type);
    input.setAttribute('placeholder', placeholder);
    return input;
}

function createButton(type, className, text) {
    const btn = document.createElement('button');
    btn.classList.add('btn', className);
    btn.setAttribute('type', type);
    btn.innerText = text;
    return btn;
}

form.append(formGroupInputEmail);
formGroupInputEmail.append(
  createLabel('email', 'Электропочта'),
  createInput('email', 'email', 'Введите свою электропочту')
);
form.append(formGroupInputPass);
formGroupInputPass.append(
  createLabel('password', 'Пароль'),
  createInput('password', 'password', 'Введите пароль')
);
form.append(formGroupCheckbox);
formGroupCheckbox.append(
  createInput('exampleCheck1', 'checkbox', 'Введите пароль', 'form-check-input'),
  createLabel('exampleCheck1', 'Запомнить меня', 'form-check-label')
);
form.append(formGroupBtn);
formGroupBtn.append(
  createButton('submit', 'btn-primary', 'Вход')
);


