/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
 */

const form = document.getElementById('form');

const formGroupInputEmail = document.createElement('div');
formGroupInputEmail.classList.add('form-group');
const formGroupInputPass = document.createElement('div');
formGroupInputPass.classList.add('form-group');
const formGroupCheckbox = document.createElement('div');
formGroupCheckbox.classList.add('form-group', 'form-check');
const formGroupBtn = document.createElement('div');
formGroupBtn.classList.add('form-group');

function createLabel(attrFor, text, className) {
    const label = document.createElement('label');
    label.classList.add(className);
    label.setAttribute('for', attrFor);
    label.innerText = text;
    return label;
}

function createInput(id, type, placeholder, className = 'form-control') {
    const input = document.createElement('input');
    input.classList.add(className);
    input.setAttribute('id', id);
    input.setAttribute('type', type);
    input.setAttribute('placeholder', placeholder);
    return input;
}

function createButton(type, className, text) {
    const btn = document.createElement('button');
    btn.classList.add('btn', className);
    btn.setAttribute('type', type);
    btn.innerText = text;
    return btn;
}

form.append(formGroupInputEmail);
formGroupInputEmail.append(
  createLabel('email', 'Электропочта'),
  createInput('email', 'email', 'Введите свою электропочту')
);
form.append(formGroupInputPass);
formGroupInputPass.append(
  createLabel('password', 'Пароль'),
  createInput('password', 'password', 'Введите пароль')
);
form.append(formGroupCheckbox);
formGroupCheckbox.append(
  createInput('exampleCheck1', 'checkbox', 'Введите пароль', 'form-check-input'),
  createLabel('exampleCheck1', 'Запомнить меня', 'form-check-label')
);
form.append(formGroupBtn);
formGroupBtn.append(
  createButton('submit', 'btn-primary', 'Вход')
);
// РЕШЕНИЕ
const submitBtn = document.querySelector('.btn')

function submitForm(event) {
    event.preventDefault();
    const inputEmail = document.querySelector('#email');
    const inputPass = document.querySelector('#password');
    const inputCheckbox = document.querySelector('#exampleCheck1');
    const obj = {
        email: inputEmail.value,
        password: inputPass.value,
        remember: inputCheckbox.checked
    }
    if (inputEmail.value === '' || inputPass.value === '') {
        throw new Error('input empty');
    }
    inputEmail.value = '';
    inputPass.value = '';
    inputCheckbox.checked = false;
    console.log(obj);
}

submitBtn.addEventListener('click', submitForm);


