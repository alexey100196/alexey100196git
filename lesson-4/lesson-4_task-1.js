/**
 * Задача 1.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!',2, 3, 512, '#', 'До свидания!'];

// Решение

function filter (arr, cb) {
    if (!Array.isArray(arr) || typeof cb !== 'function') {
        throw new Error('not Array');
    }

    const filteredArray = [];

    arr.forEach((item, index, arr) => {
        const callback = cb(item, index, arr);
        if (callback) {
            filteredArray.push(item);
        }
    });


    // for(let i = 0; i < arr.length; i++) {
    //     const element = arr[i];
    //     const callback = cb(arr[i], i, arr);
    //     if (callback) {
    //         filteredArray.push(element);
    //     }
    // }

    return filteredArray;
}

const filteredArray = filter(array, (element, index, arrayRef) => {
    console.log(`${index}:`, element, arrayRef);

    return element === 'Добрый вечер!';
});

console.log(filteredArray); // ['Добрый вечер!']

