/**
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

//array.from
function createArray(value, amount) {
    if (typeof value !== 'number' && typeof value !== 'string' && typeof value !== {} && !Array.isArray(value) || (typeof amount !== 'number')) {
        throw new Error('wrong type');
    }
    const result = new Array(amount).fill(value);
    // for (let i = 0; i < amount; i++) {
    //     result.fill(value);
    // }
    return result;
}

const result = createArray('x', 5);
console.log(result); // [ x, x, x, x, x ]

// exports.createArray = createArray;
